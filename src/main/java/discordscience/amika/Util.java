/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika;

import java.time.*;
import java.util.*;
import com.github.fcannizzaro.material.Colors;
import net.dv8tion.jda.core.entities.*;
import com.vdurmont.emoji.*;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class Util {

    public static String getMessageContent(Message message) {
        if(message.getContentRaw().length() > 0) {
            return message.getContentRaw();
        }
        String content = "";
        for(MessageEmbed embed : message.getEmbeds()) {
            if(embed.getType() != EmbedType.RICH && embed.getType() != EmbedType.IMAGE) {
                continue;
            }
            if(embed.getAuthor() != null) {
                content += "Author: " + embed.getAuthor().getName() + "\n";
            }
            if(embed.getColor() != null) {
                content += "Color: " + String.format("#%06x", embed.getColor().getRGB() & 0x00FFFFFF) + "\n";
            }
            if(embed.getDescription() != null) {
                content += "Description: " + embed.getDescription() + "\n";
            }
            for(MessageEmbed.Field field : embed.getFields()) {
                content += (field.isInline() ? "Inline" : "Non-inline") + " field '" + field.getName() + "': " + field.getValue() + "\n";
            }
            if(embed.getFooter() != null) {
                content += "Footer: " + embed.getFooter().getText() + "\n";
            }
            if(embed.getImage() != null) {
                content += "Image: " + embed.getImage().getProxyUrl() + "\n";
            }
            if(embed.getSiteProvider() != null) {
                content += "Provider: " + embed.getSiteProvider().getName() + "\n";
            }
            if(embed.getTitle() != null) {
                content += "Title: " + embed.getTitle() + "\n";
            }
        }
        return content;
    }

    public static String prettyDate(OffsetDateTime t) {
        return t.toInstant().toString().replace('T', ' ').split("\\.")[0];
    }

    public static String emojiToUrl(STBot bot, String emoji) {
        String url;
        if(bot.getJda().getEmotesByName(emoji, true).size() != 0) {
            url = bot.getJda().getEmotesByName(emoji, true).get(0).getImageUrl();
        } else {
            url = EmojiManager.getForAlias(emoji).getHtmlHexadecimal();
            url = "https://cdn.rawgit.com/twitter/twemoji/gh-pages/2/72x72/" + url.substring(3, url.length() - 1) + ".png";
        }
        return url;
    }

    public static String prettyDuration(OffsetDateTime t1, OffsetDateTime t2) {
        long time = Math.abs(t2.toInstant().toEpochMilli() - t1.toInstant().toEpochMilli()) / 1000;
        long t = 0;
        List<String> r = new ArrayList<String>();

        // Year
        t = time / (60 * 60 * 24 * 365);
        if(t > 0) {
            r.add(t + " years");
            time = time - (60 * 60 * 24 * 365 * t);
        }
        // Days
        t = time / (60 * 60 * 24);
        if(t > 0) {
            r.add(t + " days");
            time = time - (60 * 60 * 24 * t);
        }
        // Hours
        t = time / (60 * 60);
        if(t > 0) {
            r.add(t + " hours");
            time = time - (60 * 60 * t);
        }
        // Minutes
        t = time / (60);
        if(t > 0) {
            r.add(t + " minutes");
            time = time - (60 * t);
        }
        // Seconds
        t = time;
        if(t > 0) {
            r.add(t + " seconds");
        }
        return String.join(", ", r);
    }

    public static Member resolveMember(Message message, String arg) {
        if(message.getMentionedUsers().size() != 0) {
            if(message.getGuild().isMember(message.getMentionedUsers().get(0))) {
                return message.getGuild().getMember(message.getMentionedUsers().get(0));
            }
        }
        try {
            return message.getGuild().getMemberById(Long.parseLong(arg));
        } catch (NumberFormatException e) {
            arg = arg.toLowerCase();
            for(Member ms : message.getGuild().getMembers()) {
                if(ms.getEffectiveName().toLowerCase().startsWith(arg)) {
                    return ms;
                }
            }
        }
        return null;
    }

    public static User resolveUser(Message message, String arg) {
        if(message.getMentionedUsers().size() != 0) {
            return message.getMentionedUsers().get(0);
        }
        try {
            return message.getGuild().getJDA().getUserById(Long.parseLong(arg));
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
