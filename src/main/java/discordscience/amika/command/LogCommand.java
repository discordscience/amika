/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import discordscience.amika.storage.*;
import java.time.*;
import java.util.*;
import java.util.concurrent.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class LogCommand extends Command {

    private final HashMap<String, String> nameCache;

    public LogCommand(STBot bot) {
        super(bot, "log", Permission.ADMINISTRATOR);
        nameCache = new HashMap<String, String>();
        int delay = getBot().getStorageManager().getStorage("configuration").getInteger("log-delay");
        getBot().getScheduler().scheduleAtFixedRate(new MessageLogTask(), 5, delay, TimeUnit.SECONDS);
        log.debug("SCHEDULER \tScheduled repeating task: \tMessageLogTask \tDelay: " + delay + "s");
    }

    @Override
    public void executeCommand(Message message, String msg) {
        String[] args = parseArgs(msg);
        if(args.length == 0) {
            invalidUsage(message.getChannel());
            return;
        }
        //if(args[0].equalsIgnoreCase("list")) {
        //} else if(args.length == 2 && args[0].equalsIgnoreCase("remove")) {
        //} else {
        invalidUsage(message.getChannel());
        //}
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Message message = event.getMessage();
        getBot().getStorageManager().getStorage("messages").setList(message.getId(), Arrays.asList(message.getAuthor().getId(), message.getChannel().getId(), Util.getMessageContent(message)));
        nameCache.put(message.getAuthor().getId(), event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator());
    }

    @Override
    public void onCommandExecute(Message message, String msg) {
        logEvent(EventType.EXECUTE_COMMAND, message.getGuild(), message, msg);
    }

    @Override
    public void onGuildMessageUpdate(GuildMessageUpdateEvent event) {
        Message message = event.getMessage();
        // Update index
        getBot().getStorageManager().getStorage("messages").setList(message.getId(), Arrays.asList(message.getAuthor().getId(), message.getChannel().getId(), Util.getMessageContent(message)));
    }

    @Override
    public void onGuildMessageDelete(GuildMessageDeleteEvent event) {
        logEvent(EventType.MESSAGE_DELETE, event.getGuild(), event.getChannel(), event.getMessageId());
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        logEvent(EventType.MEMBER_JOIN, event.getGuild(), event.getMember());
    }

    @Override
    public void onGuildMemberLeave(GuildMemberLeaveEvent event) {
        logEvent(EventType.MEMBER_LEAVE, event.getGuild(), event.getMember());
        nameCache.put(event.getMember().getUser().getId(), event.getMember().getUser().getName() + "#" + event.getMember().getUser().getDiscriminator());
    }

    @Override
    public void onGuildBan(GuildBanEvent event) {
        logEvent(EventType.MEMBER_BAN, event.getGuild(), event.getUser());
        nameCache.put(event.getUser().getId(), event.getUser().getName() + "#" + event.getUser().getDiscriminator());
    }

    @Override
    public void onGuildUnban(GuildUnbanEvent event) {
        logEvent(EventType.MEMBER_UNBAN, event.getGuild(), event.getUser());
    }

    public void logEvent(EventType type, Guild guild, Object... args) {
        // Create log entry and set title and color
        EmbedBuilder eb = new EmbedBuilder();
        eb.setAuthor(type.getTitle(), null, Util.emojiToUrl(getBot(), type.getEmoji()));
        if(type.getColors() != null) {
            eb.setColor(type.getColors().asColor());
        }

        String content = "", footer = "";
        User user = null;
        String id = null;
        if( type == EventType.EXECUTE_COMMAND) {
            Message message = (Message) args[0];
            user = message.getAuthor();
            content += "**Command:** ```fix\n" + ((String) args[1]) + " ```";
            footer = "Msg ID: " + message.getId();
        } else if( type == EventType.BLACKLIST_TRIGGER) {
            Message message = (Message) args[0];
            user = message.getAuthor();
            if(guild == null) {
                List<Guild> guilds = message.getJDA().getMutualGuilds(user);
                if(guilds.size() > 0) {
                    guild = guilds.get(0);
                }
            }
            content += "**Content:** ```fix\n" + ((String) args[1]) + " ```";
            footer = "Msg ID: " + message.getId();
        } else if( type == EventType.MESSAGE_DELETE) {
            TextChannel channel = (TextChannel) args[0];
            String messageId = (String) args[1];
            List<String> entry = getBot().getStorageManager().getStorage("messages").getList(messageId, String.class);
            if(entry == null || entry.size() == 0) {
                entry = Arrays.asList("0", "0", "--- NON-INDEXED MESSAGE ---");
            }
            user = getBot().getJda().getUserById(id = entry.get(0));
            content += "**Channel:** " + channel.getName() + " (" + channel.getId() + ") " + channel.getAsMention() + "\n";
            content += "**Content:** ```fix\n" + entry.get(2) + " ```";
            footer = "Msg ID: " + messageId;
            // Afterwards, remove it from the index.
            getBot().getStorageManager().getStorage("messages").remove(messageId);
        } else if( type == EventType.MEMBER_JOIN) {
            Member member = (Member) args[0];
            user = member.getUser();
            content += "**Members:** " + member.getGuild().getMembers().size();
        } else if( type == EventType.MEMBER_LEAVE) {
            Member member = (Member) args[0];
            user = member.getUser();
            content += "**Members:** " + member.getGuild().getMembers().size() + "\n";
            content += "**Joined:** " + Util.prettyDate(member.getJoinDate()) + "\n";
            content += "**Duration:** " + Util.prettyDuration(member.getJoinDate(), OffsetDateTime.now());
        } else if( type == EventType.MEMBER_BAN || type == EventType.MEMBER_UNBAN ) {
            user = ((User) args[0]);
        } else if( type == EventType.COMMAND_MUTE || type == EventType.COMMAND_UNMUTE ) {
            User target = ((Member)args[0]).getUser();
            content += "**Target:** " + target.getName() + "#" + target.getDiscriminator() + " (" + target.getId() + ") " + target.getAsMention() + "\n";
            user = ((User) args[1]);
        }

        // Set content
        if(user == null) {
            content = "**User:** " + nameCache.get(id) + " (" + id + ")\n" + content;
        } else {
            content = "**User:** " + user.getName() + "#" + user.getDiscriminator() + " (" + user.getId() + ") " + user.getAsMention() + "\n" + content;
        }
        eb.setDescription(content);

        // Set timestamp
        eb.setFooter(footer + (footer.length() != 0 ? " | " : "") + Util.prettyDate(OffsetDateTime.now()), null);

        // Get the logging channel
        if(guild != null) {
            List<TextChannel> cl = guild.getTextChannelsByName(getBot().getStorageManager().getStorage("configuration").getString("log-channel"), false);
            if(cl.size() == 0) {
                log.warn("Missing logging channel. Please create a channel with the name '" + getBot().getStorageManager().getStorage("configuration").getString("log-channel") + "'.");
                return;
            }
            TextChannel logchannel = cl.get(0);
            var logmessage = eb.build();
            final long ttt = System.currentTimeMillis();
            log.debug("LOG \tSending: " + ttt);
            logchannel.sendMessage(logmessage).queue((m) -> log.debug("LOG \tSent: " + ttt + " " + m.getId()));
        }
    }

    @RequiredArgsConstructor
    public enum EventType {
        EXECUTE_COMMAND("Executed Command", Colors.green_500, "computer"),
        MESSAGE_DELETE("Message Deleted", Colors.blue_500, "wastebasket"),
        BLACKLIST_TRIGGER("Blacklist Triggered", Colors.red_500, "children_crossing"),
        MEMBER_JOIN("Member Joined", Colors.amber_500, "sparkler"),
        MEMBER_LEAVE("Member Left", Colors.brown_500, "ghost"),
        MEMBER_UNBAN("Member Unbanned", Colors.pink_500, "angel"),
        MEMBER_BAN("Member Banned", Colors.grey_900, "skull"),
        COMMAND_UNMUTE("Member Unmuted", Colors.pink_900, "speaking_head"),
        COMMAND_MUTE("Member Muted", Colors.grey_900, "no_mobile_phones");

        @Getter
        private final String title;
        @Getter
        private final Colors colors;
        @Getter
        private final String emoji;
    }

    public class MessageLogTask implements Runnable {

        private List<TextChannel> channels = null;
        private Map<String, String> progress = new HashMap<>();
        private String messageId = "";

        @Override
        public void run() {
            if(getBot().getJda() == null) {
                return; // do not run if Discord hasn't been init yet
            }
            try {
                if(channels == null) {
                    channels = new ArrayList<>();
                    for(Guild guild : getBot().getJda().getGuilds()) {
                        channels.addAll(guild.getTextChannels());
                    }
                }
                if(channels.size() == 0) {
                    if(progress != null) {
                        progress = null;
                        log.debug("LOG-FETCH \tDONE");
                    }
                    return;
                }
                log.debug("LOG-FETCH \tITERATING \t" + channels.size());
                Storage messages = getBot().getStorageManager().getStorage("messages");
                messages.optimize();
                for(Iterator<TextChannel> iterator = channels.iterator(); iterator.hasNext();) {
                    TextChannel tc = iterator.next();
                    if(tc.getId().equals(getBot().getStorageManager().getStorage("configuration").getString("log-channel"))) {
                        continue;
                    }
                    MessageHistory mh;
                    if(progress.containsKey(tc.getId())) {
                        mh = tc.getHistoryAround(progress.get(tc.getId()), 100).complete();
                    } else {
                        mh = tc.getHistory();
                    }
                    int size;
                    for(int i = 0; i != 10; i++) {
                        size = mh.size();
                        mh.retrievePast(100).complete();
                        if(size == mh.size()) {
                            break;
                        }
                    }
                    for(Message message : mh.getRetrievedHistory()) {
                        if(!messages.contains(message.getId())) {
                            messages.setList(message.getId(), Arrays.asList(message.getAuthor().getId(), message.getChannel().getId(), Util.getMessageContent(message)));
                        }
                    }
                    log.debug("LOG-FETCH \t" + tc.getGuild().getId() + ", " + tc.getId() + ", " + mh.size());
                    if(mh.size() < 700 || (OffsetDateTime.now().toEpochSecond() - mh.getRetrievedHistory().get(mh.size() - 1).getCreationTime().toEpochSecond() > (86400 * getBot().getStorageManager().getStorage("configuration").getInteger("log-days")))) {
                        iterator.remove();
                    } else {
                        progress.put(tc.getId(), mh.getRetrievedHistory().get(mh.size() - 1).getId());
                    }
                }
            } catch (Exception e) {
                log.fatal("Error while running MessageLogTask", e);
            }
        }

    }

}
