/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import java.time.*;
import java.util.*;
import java.util.stream.*;
import java.util.concurrent.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class PermissionsCommand extends Command {

    public PermissionsCommand(STBot bot) {
        super(bot, "perms", Permission.MESSAGE_MANAGE);
        addUsage("category <channel-id> [user-id]", "Displays what permissions a given user has in a given category.");
        addUsage("text <channel-id> [user-id]", "Displays what permissions a given user has in a given text channel.");
        addUsage("voice <channel-id> [user-id]", "Displays what permissions a given user has in a given voice channel.");
        addUsage("guild [user-id]", "Displays what permissions a given user has in the guild.");
    }

    @Override
    public void executeCommand(Message message, String msg) {
        String[] args = parseArgs(msg);
        if(args.length == 0) {
            invalidUsage(message.getChannel());
            return;
        }
        EmbedBuilder eb = new EmbedBuilder().setAuthor("Permission Report", null, Util.emojiToUrl(getBot(), "bookmark")).setColor(Colors.purple_500.asColor());
        List<String> content = new ArrayList<>(Arrays.asList("```diff"));
        String extra = "";
        int m = (args[0].equalsIgnoreCase("guild") ? 2 : 3);
        final Member member = (args.length == m ? message.getGuild().getMemberById(args[m - 1]) : message.getMember());
        final Channel c;
        if(args[0].equalsIgnoreCase("category")) {
            c = message.getGuild().getCategoryById(args[1]);
        } else if (args[0].equalsIgnoreCase("text")) {
            c = message.getGuild().getTextChannelById(args[1]);
        } else if (args[0].equalsIgnoreCase("voice")) {
            c = message.getGuild().getVoiceChannelById(args[1]);
        } else {
            c = null;
        }
        if(c == null && !args[0].equalsIgnoreCase("guild")) {
            invalidUsage(message.getChannel());
            return;
        }
        if(args[0].equalsIgnoreCase("category")) {
            extra = " in " + c.getName();
            content.add("  Category Permissions:");
            Permission.getPermissions(Permission.ALL_CHANNEL_PERMISSIONS).forEach(p -> content.add((member.hasPermission(c, p) ? "+ " : "- ") + p.getName()));
        } else if (args[0].equalsIgnoreCase("text")) {
            extra = " in " + c.getName();
            content.add("  Text Permissions:");
            Permission.getPermissions(Permission.ALL_TEXT_PERMISSIONS).forEach(p -> content.add((member.hasPermission(c, p) ? "+ " : "- ") + p.getName()));
        } else if (args[0].equalsIgnoreCase("voice")) {
            extra = " in " + c.getName();
            content.add("  Voice Permissions:");
            Permission.getPermissions(Permission.ALL_VOICE_PERMISSIONS).forEach(p -> content.add((member.hasPermission(c, p) ? "+ " : "- ") + p.getName()));
        } else if (args[0].equalsIgnoreCase("guild")) {
            content.add("  Guild Permissions:");
            Permission.getPermissions(Permission.ALL_GUILD_PERMISSIONS).forEach(p -> content.add((member.hasPermission(p) ? "+ " : "- ") + p.getName()));
        }

        content.add("```");
        eb.addField("Permissions for " + member.getUser().getName() + "#" + member.getUser().getDiscriminator() + " <" + member.getUser().getId() + ">", String.join("\n", content), false);
        eb.setFooter("Requested by: " + message.getAuthor().getName() + "#" + message.getAuthor().getDiscriminator() + " <" + message.getAuthor().getId() + "> | " + Util.prettyDate(OffsetDateTime.now()), null);
        message.getChannel().sendMessage(eb.build()).queue();
    }

}
