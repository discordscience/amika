/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import java.util.*;
import java.util.stream.*;
import java.util.concurrent.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class AutoDeleteCommand extends Command {

    @Getter @Setter
    private long holdTime = 0;

    public AutoDeleteCommand(STBot bot) {
        super(bot, "autodelete", Permission.MESSAGE_MANAGE);
        addUsage("list", "Displays a list of channels where messages are automatically deleted.");
        addUsage("add <channel> <duration>", "Adds a channel to the autodelete list. Duration in seconds.");
        addUsage("remove <channel>", "Removes a channel from the autodelete list.");
    }

    @Override
    public void executeCommand(Message message, String msg) {
        String[] args = parseArgs(msg);
        if(args.length == 0) {
            invalidUsage(message.getChannel());
            return;
        }
        if(args[0].equalsIgnoreCase("list")) {
            EmbedBuilder eb = new EmbedBuilder().setColor(Colors.blue_grey_500.asColor()).setTitle("\uD83D\uDDD2 AutoDelete List");
            String list1 = "", list2 = "";
            Map<String, Integer> channels = getBot().getStorageManager().getStorage("storage").getMap("autodelete-channel-list", Integer.class);
            for(Map.Entry<String, Integer> e : channels.entrySet()) {
                list1 += e.getKey() + "\n";
                list2 += e.getValue() + "\n";
            }
            eb.addField("Channel ID", list1, true);
            eb.addField("Duration in seconds", list2, true);
            message.getChannel().sendMessage(eb.build()).queue();
        } else if(args.length == 3 && args[0].equalsIgnoreCase("add")) {
            Map<String, Integer> channels = getBot().getStorageManager().getStorage("storage").getMap("autodelete-channel-list", Integer.class);
            int duration = 0;
            try {
                duration = Integer.parseInt(args[2]);
            } catch(Exception e) {
                invalidUsage(message.getChannel());
                return;
            }
            channels.put(args[1].trim(), duration);
            getBot().getStorageManager().getStorage("storage").setMap("autodelete-channel-list", channels);
            message.getChannel().sendMessage("Channel added to autodelete list.").queue();
        } else if(args.length == 2 && args[0].equalsIgnoreCase("remove")) {
            Map<String, Integer> channels = getBot().getStorageManager().getStorage("storage").getMap("autodelete-channel-list", Integer.class);
            channels.remove(args[1].trim());
            getBot().getStorageManager().getStorage("storage").setMap("autodelete-channel-list", channels);
            message.getChannel().sendMessage("Channel removed from autodelete list.").queue();
        } else {
            invalidUsage(message.getChannel());
        }
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        if(message.getGuild() == null) {
            return;
        }
        if((System.currentTimeMillis() - holdTime) / 1000 < 60) {
            return;
        }
        Map<String, Integer> channels = getBot().getStorageManager().getStorage("storage").getMap("autodelete-channel-list", Integer.class);
        String id = message.getChannel().getId();
        if(channels.containsKey(id)) {
            int delay = channels.get(id);
            getBot().getScheduler().schedule(() -> message.delete().queue(), delay, TimeUnit.SECONDS);
        }
    }

}
