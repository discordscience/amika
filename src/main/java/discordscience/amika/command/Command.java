/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.hooks.*;
import java.util.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
@RequiredArgsConstructor
public abstract class Command extends ListenerAdapter {

    @NonNull
    @Getter
    private final STBot bot;
    @NonNull
    @Getter
    private final String name;
    @NonNull
    @Getter
    private final Permission permission;
    private final List<String> usage = new ArrayList<>();

    public void executeCommand(Message message, String msg) {
    }

    public void onCommandExecute(Message message, String msg) {
    }

    public final void printUsage(MessageChannel channel) {
        if(usage.size() == 0) {
            channel.sendMessage(new EmbedBuilder().setColor(Colors.yellow_500.asColor()).setDescription("\u26A0 **Warning**\nThe command `" + name + "` is missing usage documentation. \uD83D\uDE26").build()).queue();
            log.warn("Missing usage for command '" + name + "'.");
        } else {
            channel.sendMessage(new EmbedBuilder().setColor(Colors.cyan_500.asColor()).setDescription("\uD83D\uDCD8 **Usage**\n" + String.join("\n", usage)).build()).queue();
        }
    }

    public final void invalidUsage(MessageChannel channel) {
        // Print help if enabled
        for(Command cmd : getBot().getCommands()) {
            if(cmd.getName().equals("help")) {
                ((HelpCommand) cmd).printCommandHelp(channel, getName());
                return;
            }
        }

        channel.sendMessage(new EmbedBuilder().setColor(Colors.red_500.asColor()).setDescription("\uD83D\uDD34 **Incorrect usage.**\nIncorrect usage. Please do `" + getBot().getStorageManager().getStorage("configuration").getString("command-prefix") + "help " + getName() + "` for more information.").build()).queue();
    }

    public final void addUsage(String sub, String description) {
        usage.add("`" + getBot().getStorageManager().getStorage("configuration").getString("command-prefix") + getName() + " " + sub + "` - " + description);
    }

    public final String[] parseArgs(String msg) {
        String[] args = msg.split(" ");
        return Arrays.copyOfRange(args, 1, args.length);
    }
}
