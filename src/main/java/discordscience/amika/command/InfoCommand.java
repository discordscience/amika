/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import java.time.*;
import java.util.*;
import java.util.stream.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class InfoCommand extends Command {

    public InfoCommand(STBot bot) {
        super(bot, "info", Permission.UNKNOWN);
        addUsage("user [mention/user-id/username]", "Displays information about yourself o another user.");
        addUsage("server", "Displays information about this server.");
        addUsage("bot", "Displays statistics and information about myself.");
    }

    @Override
    public void executeCommand(Message message, String msg) {
        String[] args = parseArgs(msg);
        if(args.length == 0 || args[0].length() == 0) {
            invalidUsage(message.getChannel());
            return;
        }
        String content = "";
        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Colors.amber_500.asColor());
        if(args[0].equalsIgnoreCase("user") || args[0].equalsIgnoreCase("member")) {
            Member member = null;
            User randomUser = null;
            if(args.length >= 2) {
                member = Util.resolveMember(message, args[1]);
                if(member == null) {
                    randomUser = Util.resolveUser(message, args[1]);
                }
            } else {
                member = message.getMember();
            }
            if(member == null && randomUser == null) {
                invalidUsage(message.getChannel());
                return;
            }
            if(member != null) {
                eb.setAuthor("Member Info: " + member.getUser().getName() + "#" + member.getUser().getDiscriminator(), null, Util.emojiToUrl(getBot(), "golfer"));
                eb.addField("ID: (Snowflake)", member.getUser().getId(), true);
                eb.addField("User:", member.getAsMention(), true);
                eb.addField("Nickname:", member.getEffectiveName(), true);
                eb.addField("Color:", member.getColor() == null ? "null" : String.format("#%06x", member.getColor().getRGB() & 0x00FFFFFF), true);
                eb.addField("Joined at:", Util.prettyDate(member.getJoinDate()), true);
                eb.addField("Created at:", Util.prettyDate(member.getUser().getCreationTime()), true);
                eb.addField("Roles:", String.join(", ", member.getRoles().stream().map(Role::getName).collect(Collectors.toList())), false);
            } else {
                eb.setAuthor("User (not member) Info: " + randomUser.getName() + "#" + randomUser.getDiscriminator(), null, Util.emojiToUrl(getBot(), "cartwheel"));
                eb.addField("ID: (Snowflake)", randomUser.getId(), true);
                eb.addField("User:", member.getAsMention(), true);
                eb.addField("Created at:", Util.prettyDate(randomUser.getCreationTime()), true);
            }
        } else if(args[0].equalsIgnoreCase("guild") || args[0].equalsIgnoreCase("server")) {
            Guild guild = message.getGuild();
            eb.setAuthor("Guild Info: " + guild.getName(), null, Util.emojiToUrl(getBot(), "house"));
            eb.addField("ID: (Snowflake)", guild.getId(), true);
            eb.addField("Created at", Util.prettyDate(guild.getCreationTime()), true);
            eb.addField("Owned by:", guild.getOwner().getAsMention(), true);
            eb.addField("Members:", guild.getMembers().size() + "", true);
            eb.addField("Roles:", guild.getRoles().size() + "", true);
            eb.addField("Server Region", guild.getRegionRaw(), true);
        } else if(args[0].equalsIgnoreCase("bot")) {
            eb.setAuthor("Myself", null, Util.emojiToUrl(getBot(), "books"));
            eb.addField("My ID: (Snowflake)", getBot().getJda().getSelfUser().getId(), true);
            eb.addField("Software:", "STBot v" + STBot.class.getPackage().getImplementationVersion(), true);
            eb.addField("Uptime:", Util.prettyDuration(getBot().getStartTime(), OffsetDateTime.now()), true);
            eb.addField("Guilds:", getBot().getJda().getGuilds().size() + " guilds", true);
            eb.addField("Global users:", getBot().getJda().getUsers().size() + " users", true);
            eb.addField("Ping:", getBot().getJda().getPing() + "ms", true);
        } else {
            invalidUsage(message.getChannel());
            return;
        }
        eb.setDescription(content);
        eb.setFooter("Requested by: " + message.getAuthor().getName() + "#" + message.getAuthor().getDiscriminator() + " | " + Util.prettyDate(OffsetDateTime.now()), null);
        message.getChannel().sendMessage(eb.build()).queue();
    }

}
