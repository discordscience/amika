/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import java.time.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class UnmuteCommand extends Command {

    public UnmuteCommand(STBot bot) {
        super(bot, "unmute", Permission.BAN_MEMBERS);
        addUsage("<user>", "Unmutes the target user. Mute with `mute` command.");
    }

    @Override
    public void executeCommand(Message message, String msg) {
        // Check args
        String[] args = parseArgs(msg);
        if(args.length == 0 || args[0].length() == 0) {
            invalidUsage(message.getChannel());
            return;
        }

        // Find member
        Member member = Util.resolveMember(message, args[0]);
        if(member == null) {
            invalidUsage(message.getChannel());
            return;
        }

        // Delete command message
        getBot().getScheduler().schedule(() -> message.delete().queue(), 5, TimeUnit.SECONDS);

        // Unmute member
        Role mutedRole = message.getGuild().getRoleById(getBot().getStorageManager().getStorage("configuration").getLong("muted-role"));
        if(!member.getRoles().contains(mutedRole)) {
            message.getChannel().sendMessage("Member is not muted! ").queue((nm) -> getBot().getScheduler().schedule(() -> nm.delete().queue(), 15, TimeUnit.SECONDS));
            return;
        }

        if(!message.getMember().canInteract(member)) {
            message.getChannel().sendMessage("You do not have permission to unmute this user!").queue((nm) -> getBot().getScheduler().schedule(() -> nm.delete().queue(), 15, TimeUnit.SECONDS));
            return;
        }
        message.getGuild().getController().removeSingleRoleFromMember(member, mutedRole).complete();

        message.getChannel().sendMessage("Member unmuted! " + member.getUser().getName() + "#" + member.getUser().getDiscriminator()).queue((nm) -> getBot().getScheduler().schedule(() -> nm.delete().queue(), 15, TimeUnit.SECONDS));

        // Send event to log-command if enabled
        for(Command command : getBot().getCommands()) {
            if(command.getName().equals("log")) {
                ((LogCommand) command).logEvent(LogCommand.EventType.COMMAND_UNMUTE, message.getGuild(), member, message.getAuthor());
            }
        }
    }

}
