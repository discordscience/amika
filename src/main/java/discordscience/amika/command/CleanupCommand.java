/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import java.time.*;
import java.util.*;
import java.util.concurrent.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class CleanupCommand extends Command {

    public CleanupCommand(STBot bot) {
        super(bot, "cleanup", Permission.MESSAGE_MANAGE);
        addUsage("count <count>", "Deletes X amount of messages.");
    }

    @Override
    public void executeCommand(Message message, String msg) {
        String[] args = parseArgs(msg);
        if(args.length == 0) {
            invalidUsage(message.getChannel());
            return;
        }
        if(args.length == 2 && args[0].equalsIgnoreCase("count")) {
            int count = -1;
            try {
                count = Integer.parseInt(args[1]);
            } catch(Exception e) {
                invalidUsage(message.getChannel());
                return;
            }
            message.delete().complete();
            if(count < 2 || count > 100) {
                message.getChannel().sendMessage(new EmbedBuilder().setColor(Colors.red_500.asColor()).setTitle("\uD83D\uDD34 Incorrect usage").setDescription("Please enter a number between 2 and 100.").build()).queue();
                return;
            }

            message.getChannel().getHistory().retrievePast(count).queue(list -> {
                int total = list.size();
                List<Message> bulk = new ArrayList<>();
                for(Message m : list) {
                    if(OffsetDateTime.now().toEpochSecond() - m.getCreationTime().toEpochSecond() > (86400 * 13)) {
                        m.delete().queue();
                    } else {
                        bulk.add(m);
                    }
                }
                message.getTextChannel().deleteMessages(list).queue((Void) -> {
                    message.getChannel().sendMessage("I have deleted " + total + " messages.").queue((nm) -> {
                        getBot().getScheduler().schedule(() -> nm.delete().queue(), 5, TimeUnit.SECONDS);
                    });
                });
            });

        } else {
            invalidUsage(message.getChannel());
        }
    }

}
