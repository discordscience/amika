/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import java.time.*;
import java.util.*;
import java.util.concurrent.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class BanCommand extends Command {

    public BanCommand(STBot bot) {
        super(bot, "ban", Permission.BAN_MEMBERS);
        addUsage("<id> <delDays>", "Ban specific member by ID.");
    }

    @Override
    public void executeCommand(Message message, String msg) {
        String[] args = parseArgs(msg);
        if(args.length == 0) {
            invalidUsage(message.getChannel());
            return;
        }
        if(args.length == 2) {
            int delDays = -1;
            try {
                delDays = Integer.parseInt(args[1]);
            } catch(Exception e) {
                invalidUsage(message.getChannel());
                return;
            }

            Member member = Util.resolveMember(message, args[0]);
            long id = -1;
            if(member != null) {
                if(!message.getMember().canInteract(member)) {
                    message.getChannel().sendMessage("You do not have permission to ban this user!").queue((nm) -> getBot().getScheduler().schedule(() -> nm.delete().queue(), 15, TimeUnit.SECONDS));
                    return;
                }
                id = member.getUser().getIdLong();
            } else {
                try {
                    id = Long.parseLong(args[0]);
                } catch(Exception e) {
                    invalidUsage(message.getChannel());
                    return;
                }
            }


            try {
                message.getGuild().getController().ban(id + "", delDays).complete();
            } catch(Exception e) {
                message.getChannel().sendMessage(new EmbedBuilder().setColor(Colors.yellow_500.asColor()).setTitle("\u26A0 **Warning**\nNon-existent User!").setDescription("Check you have copied the real ID, and try again.").build()).queue();
                return;
            }

            User user = message.getAuthor();
            message.getChannel().sendMessage(new EmbedBuilder().setColor(Colors.cyan_500.asColor()).setTitle("\u2705 Banned Member").addField("Id:", id + "", true).addField("Delete days:", delDays + "", true).addField("Requested by:", user.getName() + "#" + user.getDiscriminator() + " <" + user.getId() + ">", false).build()).queue();
        } else {
            invalidUsage(message.getChannel());
        }
    }

}
