/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class BlacklistCommand extends Command {

    private final List<String> pornlist;

    @SneakyThrows
    public BlacklistCommand(STBot bot) {
        super(bot, "blacklist", Permission.MESSAGE_MANAGE);
        addUsage("list", "Displays a list of blacklisted words.");
        addUsage("add <word>", "Adds the word to the blacklist.");
        addUsage("remove <word>", "Removes the word to the blacklist.");
        pornlist = new BufferedReader(new FileReader("pornlist.txt")).lines().collect(Collectors.toList());
        log.debug("BLACKLIST \tLoaded " + pornlist.size() + " blocked explicit domains");
    }

    @Override
    public void executeCommand(Message message, String msg) {
        String[] args = parseArgs(msg);
        if(args.length == 0) {
            invalidUsage(message.getChannel());
            return;
        }
        if(args[0].equalsIgnoreCase("list")) {
            EmbedBuilder eb = new EmbedBuilder().setColor(Colors.blue_grey_500.asColor()).setTitle("\uD83D\uDEB8 Blacklist List");
            String list1 = "", list2 = "", list3 = "";
            List<String> blacklist = getBot().getStorageManager().getStorage("storage").getList("blacklist", String.class);
            for(int i = 0; i != blacklist.size(); i++) {
                switch (i % 3) {
                case 0:
                    list1 += blacklist.get(i) + "\n";
                    break;
                case 1:
                    list2 += blacklist.get(i) + "\n";
                    break;
                case 2:
                    list3 += blacklist.get(i) + "\n";
                    break;
                }
            }
            eb.addField("", list1, true);
            eb.addField("", list2, true);
            eb.addField("", list3, true);
            message.getChannel().sendMessage(eb.build()).queue();
        } else if(args.length == 2 && args[0].equalsIgnoreCase("add")) {
            List<String> blacklist = getBot().getStorageManager().getStorage("storage").getList("blacklist", String.class);
            blacklist.add(msg.split(" ", 3)[2].toLowerCase());
            Collections.sort(blacklist);
            getBot().getStorageManager().getStorage("storage").setList("blacklist", blacklist);
            message.getChannel().sendMessage("Word added to blacklist.").queue();
        } else if(args.length == 2 && args[0].equalsIgnoreCase("remove")) {
            List<String> blacklist = getBot().getStorageManager().getStorage("storage").getList("blacklist", String.class);
            blacklist.remove(msg.split(" ", 3)[2].toLowerCase());
            Collections.sort(blacklist);
            getBot().getStorageManager().getStorage("storage").setList("blacklist", blacklist);
            message.getChannel().sendMessage("Word removed to blacklist.").queue();
        } else {
            invalidUsage(message.getChannel());
        }
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        checkMessage(event.getMessage());
    }

    @Override
    public void onGuildMessageUpdate(GuildMessageUpdateEvent event) {
        checkMessage(event.getMessage());
    }

    private void checkMessage(Message message) {
        if(message.getAuthor().isBot()) {
            return;
        }
        if(message.getContentRaw().length() == 0 || message.getContentRaw().substring(1).startsWith("blacklist")) {
            return;
        }
        if(message.getGuild() != null && message.getMember().hasPermission(Permission.MESSAGE_MANAGE)) {
            return;
        }
        String[] words = message.getContentRaw().split(" ");
        List<String> blacklist = getBot().getStorageManager().getStorage("storage").getList("blacklist", String.class);
        for(String word : words) {
            String wordlc = word.toLowerCase();
            String url = wordlc.startsWith("http://") ? wordlc.substring(7) : (wordlc.startsWith("https://") ? wordlc.substring(8) : wordlc);
            boolean pornw = pornlist.stream().filter(domain -> url.startsWith(domain)).findFirst().isPresent();
            boolean discordgg = url.startsWith("discord.gg") && !url.startsWith("discord.gg/science");
            boolean discordme = url.startsWith("discord.me") && !url.startsWith("discord.me/science");
            if(blacklist.contains(wordlc.replaceAll("[^A-Za-z0-9]", "")) || discordgg || discordme || pornw) {
                // log
                log.info("{}#{} <{}> said blacklisted word \"{}\": {}", message.getAuthor().getName(), message.getAuthor().getDiscriminator(), message.getAuthor().getId(), word, message.getContentRaw());
                // delete their message
                if(message.getGuild() != null) {
                    getBot().getScheduler().schedule(() -> message.delete().queue(), 0, TimeUnit.SECONDS);
                }
                // send public warning message
                if(message.getGuild() != null) {
                    if (pornw || discordgg || discordme) {
                        message.getChannel().sendMessage(new EmbedBuilder().setColor(Colors.red_500.asColor()).setTitle("\uD83D\uDEB8 Blacklist Warning").setDescription("Your message was deleted because it contained a blacklisted link or a blacklisted domain.").build()).queue( message2 -> getBot().getScheduler().schedule(() -> message2.delete().queue(), 30, TimeUnit.SECONDS));
                        if (pornw) {
                            getBot().getJda().getGuildById("184140444677046274").getTextChannelById("389214913974894592").sendMessage("<@&218065162454827008> User " + message.getAuthor().getAsMention() + " sent a message with a pornographic link!").queue( message3 -> getBot().getScheduler().schedule(() -> message3.delete().queue(), 120, TimeUnit.SECONDS));
                        }
                    } else {
                        message.getChannel().sendMessage(new EmbedBuilder().setColor(Colors.red_500.asColor()).setTitle("\uD83D\uDEB8 Blacklist Warning").setDescription("Your message was deleted because it contained a blacklisted word.").build()).queue( message2 -> getBot().getScheduler().schedule(() -> message2.delete().queue(), 30, TimeUnit.SECONDS));
                    }
                }
                // open pm-channel and warn them
                message.getAuthor().openPrivateChannel().queue( pc -> pc.sendMessage(new EmbedBuilder().setColor(Colors.red_500.asColor()).setTitle("\uD83D\uDEB8 Blacklist Warning").setDescription("Your message was deleted because it contained the blacklisted word `" + word + "`.\n```fix\n" + message.getContentRaw() + "```\n**Please mind your language on this server and remember to read the rules.**").build()).queue());
                // Send event to log-command if enabled
                for(Command command : getBot().getCommands()) {
                    if(command.getName().equals("log")) {
                        ((LogCommand) command).logEvent(LogCommand.EventType.BLACKLIST_TRIGGER, message.getGuild(), message, word);
                    }
                }
            }
        }
    }

}
