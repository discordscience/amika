/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import java.util.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class HelpCommand extends Command {

    public HelpCommand(STBot bot) {
        super(bot, "help", Permission.UNKNOWN);
    }

    @Override
    public void executeCommand(Message message, String msg) {
        String[] args = parseArgs(msg);
        if(args.length == 0) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setAuthor("Help", null, Util.emojiToUrl(getBot(), "passport_control"));
            eb.setColor(Colors.cyan_500.asColor());
            List<String> list1 = new ArrayList<>();
            List<String> list2 = new ArrayList<>();
            for(Command cmd : getBot().getCommands()) {
                boolean perms = cmd.getPermission() == Permission.UNKNOWN || message.getMember().hasPermission(cmd.getPermission());
                if(perms) {
                    list1.add(getBot().getStorageManager().getStorage("configuration").getString("command-prefix") + cmd.getName());
                }
            }
            eb.addField("Commands", "```yml\n" + String.join("\n", list1) + "```", true);
            message.getChannel().sendMessage(eb.build()).queue();
        } else {
            printCommandHelp(message.getChannel(), args[0]);
        }
    }

    public void printCommandHelp(MessageChannel channel, String command) {
        for(Command cmd : getBot().getCommands()) {
            if(cmd.getName().equals(command)) {
                cmd.printUsage(channel);
            }
        }
    }

}
