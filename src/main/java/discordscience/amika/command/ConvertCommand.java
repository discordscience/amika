/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.command;

import discordscience.amika.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class ConvertCommand extends Command {

    public ConvertCommand(STBot bot) {
        super(bot, "convert", Permission.UNKNOWN);
        addUsage("10C F", "Converts 10 celcius to fahrenheit.");
        addUsage("10F C", "Converts 10 fahrenheit to celcius.");
        addUsage("10m yd", "Converts 10 meters to yards.");
        addUsage("10yd m", "Converts 10 yards to meters.");
        addUsage("10km mi", "Converts 10 kilometers to miles.");
        addUsage("10mi km", "Converts 10 miles to kilometers.");
    }

    @Override
    public void executeCommand(Message message, String msg) {
        String[] args = parseArgs(msg);
        if(args.length != 2) {
            invalidUsage(message.getChannel());
            return;
        }
        String p1 = args[0].toLowerCase();
        String p2 = args[1].toLowerCase();
        String ns = p1.replace(',', '.').replaceAll("[^0-9\\.]", "");
        double n;
        try {
            n = Double.parseDouble(ns);
        } catch(NumberFormatException e) {
            invalidUsage(message.getChannel());
            return;
        }
        String t1 = p1.replaceAll("[^a-z]", "");
        String t2 = p2.replaceAll("[^a-z]", "");

        DecimalFormat df = new DecimalFormat("#.##");
        EmbedBuilder eb = new EmbedBuilder().setColor(Colors.blue_500.asColor()).setTitle("\uD83D\uDCCA Convert");
        if(t1.equals("F") && t2.equals("C")) {
            double a = (n - 32) / 1.8;
            eb.addField("Fahrenheit to Celcius:", df.format(n) + "F -> " + df.format(a) + "C", false);
        } else if(t1.equals("c") && t2.equals("f")) {
            double a = (n * 1.8) + 32;
            eb.addField("Celcius to Fahrenheit:", df.format(n) + "C -> " + df.format(a) + "F", false);
        } else if(t1.equals("m") && t2.equals("yd")) {
            double a = n * 1.0936;
            eb.addField("Meters to Yards:", df.format(n) + "m -> " + df.format(a) + "yd", false);
        } else if(t1.equals("yd") && t2.equals("m")) {
            double a = n / 1.0936;
            eb.addField("Yards to Meters:", df.format(n) + "yd -> " + df.format(a) + "m", false);
        } else if(t1.equals("mi") && t2.equals("km")) {
            double a = n / 0.62137;
            eb.addField("Miles to Kilometers:", df.format(n) + "mi -> " + df.format(a) + "km", false);
        } else if(t1.equals("km") && t2.equals("mi")) {
            double a = n * 0.62137;
            eb.addField("Kilometers to Miles:", df.format(n) + "km -> " + df.format(a) + "mi", false);
        } else {
            invalidUsage(message.getChannel());
            return;
        }
        eb.setFooter("Requested by " + message.getAuthor().getName() + "#" + message.getAuthor().getDiscriminator(), null);
        message.getChannel().sendMessage(eb.build()).queue();
    }

}
