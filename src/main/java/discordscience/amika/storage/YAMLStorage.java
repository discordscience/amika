/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.storage;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import org.json.*;
import lombok.*;
import discordscience.amika.*;

@lombok.extern.log4j.Log4j2
public class YAMLStorage extends Storage {

    private JSONObject data;
    private final String fileName;

    public YAMLStorage(STBot bot, String name) {
        super(bot, name, "YAML");

        fileName = "stbot_" + getName()  + ".json";
    }

    private void generateData() {
        data.put("discord-token", "");
        data.put("command-list", new JSONArray(Arrays.asList("Ping", "Help")));
        data.put("scheduler-pool", 8);
        data.put("command-prefix", "!");
        data.put("log-channel", "stbot-log");
        data.put("log-days", 90);
        data.put("log-delay", 60);
        data.put("unknown-command", true);
    }

    @Override
    public void open() {
        File f = new File(fileName);
        if( !f.isFile() ) {
            log.warn("Missing " + fileName + " file!");
            data = new JSONObject();
            if(getName().equals("configuration")) {
                generateData();
                close();
                log.error("Generated new settings file. Please review and change appropriate settings.");
                System.exit(1);
            } else {
                log.warn("Generated new '" + getName() + "' storage file.");
            }
            close();
        }
        try {
            data = new JSONObject(new Scanner(f).useDelimiter("\\Z").next());
        } catch (Exception e) {
            log.fatal("Error while reading data storage", e);
        }
    }

    @Override
    public void close() {
        try (FileWriter file = new FileWriter(fileName)) {
            file.write(data.toString(4));
        } catch (Exception e) {
            log.fatal("Error while writing data storage", e);
        }
    }

    @Override
    public String getString(String key) {
        return data.has(key) ? data.getString(key) : null;
    }

    @Override
    public void setString(String key, String value) {
        data.put(key, value);
    }

    @Override
    public int getInteger(String key) {
        return data.has(key) ? data.getInt(key) : -1;
    }

    @Override
    public void setInteger(String key, int value) {
        data.put(key, value);
    }

    @Override
    public long getLong(String key) {
        return data.has(key) ? data.getLong(key) : -1;
    }

    @Override
    public void setLong(String key, long value) {
        data.put(key, value);
    }

    @Override
    public boolean getBoolean(String key) {
        return data.has(key) ? data.getBoolean(key) : false;
    }

    @Override
    public void setBoolean(String key, boolean value) {
        data.put(key, value);
    }

    @Override
    public List<Object> getList(String key) {
        List<Object> list = new ArrayList<>();
        if(!data.has(key)) {
            return list;
        }
        data.getJSONArray(key).forEach(list::add);
        return list;
    }

    @Override
    public void setList(String key, List<?> list) {
        data.put(key, new JSONArray(list));
    }

    @Override
    public Map<String, Object> getMap(String key) {
        Map<String, Object> map = new HashMap<>();
        if(!data.has(key)) {
            return map;
        }
        JSONObject obj = data.getJSONObject(key);
        obj.keySet().forEach(s -> map.put(s, obj.get(s)));
        return map;
    }

    @Override
    public void setMap(String key, Map<String, ?> map) {
        data.put(key, new JSONObject(map));
    }

    @Override
    public boolean contains(String key) {
        return data.has(key);
    }

    @Override
    public void remove(String key) {
        data.remove(key);
    }

    @Override
    public void optimize() {
    }

}
