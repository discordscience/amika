/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.storage;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import org.json.*;
import lombok.*;
import discordscience.amika.*;

@lombok.extern.log4j.Log4j2
@RequiredArgsConstructor
public abstract class Storage {

    @Getter
    public final STBot bot;
    @Getter
    public final String name;
    @Getter
    public final String backend;

    public abstract void open();

    public abstract void close();

    public abstract String getString(String key);

    public abstract void setString(String key, String value);

    public abstract int getInteger(String key);

    public abstract void setInteger(String key, int value);

    public abstract long getLong(String key);

    public abstract void setLong(String key, long value);

    public abstract boolean getBoolean(String key);

    public abstract void setBoolean(String key, boolean value);

    public abstract List<Object> getList(String key);

    public <A> List<A> getList(String key, Class<A> a) {
        List<Object> objs = getList(key);
        if(objs == null) {
            return null;
        }
        List<A> list = new ArrayList<>();
        Object o;
        for(int i = 0; i != objs.size(); i++) {
            o = objs.get(i);
            if(a.isInstance(o)) {
                list.add(a.cast(o));
            } else {
                throw new UnsupportedOperationException("Illegal state of data format.");
            }
        }
        return list;
    }

    public abstract void setList(String key, List<?> list);

    public abstract Map<String, Object> getMap(String key);

    public <A> Map<String, A> getMap(String key, Class<A> a) {
        Map<String, Object> objs = getMap(key);
        if(objs == null) {
            return null;
        }
        Map<String, A> map = new HashMap<>();
        Object o;
        for(String k : objs.keySet()) {
            o = objs.get(k);
            if(a.isInstance(o)) {
                map.put(k, a.cast(o));
            } else {
                throw new UnsupportedOperationException("Illegal state of data format.");
            }
        }
        return map;
    }

    public abstract void setMap(String key, Map<String, ?> map);

    public abstract boolean contains(String key);

    public abstract void remove(String key);

    public abstract void optimize();

}
