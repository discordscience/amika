/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika.storage;

import java.io.*;
import java.nio.file.*;
import java.lang.reflect.Constructor;
import java.util.*;
import org.json.*;
import lombok.*;
import discordscience.amika.*;

@lombok.extern.log4j.Log4j2
@RequiredArgsConstructor
public class StorageManager {

    private final Map<String, Storage> storages = new HashMap<>();
    private final STBot bot;

    public void close() {
        storages.values().forEach(s -> {
            if(!s.getName().equals("configuration")) {
                s.close();
            }
        });
        storages.clear();
    }

    public Storage getStorage(String name) {
        name = name.toLowerCase();
        if(!storages.containsKey(name)) {
            createStorage(name);
        }
        return storages.get(name);
    }

    public void createStorage(String name) {
        String backend;
        if(name.equals("configuration")) {
            backend = "YAML";
        } else {
            Storage settings = getStorage("configuration");
            Map<String, String> list = settings.getMap("storage-list", String.class);
            if(!list.containsKey(name)) {
                list.put(name, "YAML");
                settings.setMap("storage-list", list);
                settings.close();
                log.warn("Added a new storage '" + name + "' to the storage-list in the settings file.");
            }
            backend = list.get(name);
        }

        try {
            Class<?> clazz = Class.forName("discordscience.amika.storage." + backend + "Storage");
            Constructor<?> ctor = clazz.getConstructor(STBot.class, String.class);
            Storage storage = (Storage) ctor.newInstance(new Object[] { bot, name });
            storage.open();
            log.debug("STORAGE \tLoaded storage: \t" + name);
            storages.put(name, storage);
        } catch(Exception e) {
            log.error("Failed to load storage \"" + name + "\".", e);
            System.exit(1);
        }
    }
}
