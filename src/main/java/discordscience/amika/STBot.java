/*

    Amika Science and Technology Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.amika;

import java.time.*;
import java.util.*;
import java.util.concurrent.*;
import java.lang.reflect.Constructor;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.utils.*;
import net.dv8tion.jda.core.requests.*;
import discordscience.amika.command.*;
import discordscience.amika.storage.*;
import com.github.fcannizzaro.material.Colors;
import org.apache.logging.log4j.LogManager;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class STBot {

    public static void main(String[] args) {
        log.trace("TRACE");
        log.debug("DEBUG");
        log.error("ERROR");
        log.fatal("FATAL");
        log.info("INFO");
        log.warn("WARN");

        new STBot();
    }

    @Getter
    private final OffsetDateTime startTime = OffsetDateTime.now();
    @Getter
    private final ScheduledExecutorService scheduler;
    @Getter
    private final StorageManager storageManager;
    @Getter
    private final ArrayList<Command> commands;
    @Getter
    private JDA jda;

    public STBot() {
        log.info("===== Initialising bot =====");

        // Start storage manager
        storageManager = new StorageManager(this);

        // Load Scheduler
        scheduler = Executors.newScheduledThreadPool(getStorageManager().getStorage("configuration").getInteger("scheduler-pool"));

        log.info("===== Starting bot =====");

        // Connect to Discord
        commands = new ArrayList<>();
        try {
            JDABuilder jdab = new JDABuilder(AccountType.BOT)
            .setAudioEnabled(false)
            .setAutoReconnect(true)
            .setToken(getStorageManager().getStorage("configuration").getString("discord-token"))
            .addEventListener(new BotEventListener(this));

            // Load commands
            for (String label : getStorageManager().getStorage("configuration").getList("command-list", String.class)) {
                try {
                    Class<?> clazz = Class.forName("discordscience.amika.command." + label + "Command");
                    Constructor<?> ctor = clazz.getConstructor(STBot.class);
                    Command command = (Command) ctor.newInstance(new Object[] { this });
                    commands.add(command);
                    jdab.addEventListener(command);
                } catch(Exception e) {
                    log.error("Failed to load command \"" + label + "\".", e);
                    System.exit(1);
                }
                log.debug("COMMAND \tLoaded command: \t" + label);
            }
            log.info("Activated " + commands.size() + " commands.");

            jda = jdab.buildBlocking();
        } catch (Exception e) {
            log.fatal("Error while initializing bot", e);
            System.exit(-1);
        }

        // Setup shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                log.info("Running shutdown hook.");
                jda.shutdown();
                log.info("Saving storage.");
                getStorageManager().close();
                log.info("Shutdown hook done. Bye.");
                jda.shutdownNow();
                LogManager.shutdown();
            }
        });
    }

    public void executeCommand(Message message) {
        // Ignore messages from bots
        if(message.getAuthor().isBot()) {
            return;
        }
        // Split up into arguments
        String msg = message.getContentRaw().replace("\n", "").replace("\r", "").replace("`", "`\u200B");
        String prefix = getStorageManager().getStorage("configuration").getString("command-prefix");
        if(!msg.startsWith(prefix)) {
            return;
        }
        String name = msg.split(" ")[0].substring(1).toLowerCase();
        // Only allow commands on guilds
        if(message.getGuild() == null) {
            message.getChannel().sendMessage(new EmbedBuilder().setColor(Colors.red_500.asColor()).setTitle("\u2757 Invalid Usage").setDescription("Commands can only be executed in a guild.").build()).queue();
            return;
        }
        for(Command cmd : commands) {
            if(cmd.getName().equals(name)) {
                if(cmd.getPermission() != Permission.UNKNOWN && !message.getMember().hasPermission(cmd.getPermission())) {
                    message.getChannel().sendMessage(new EmbedBuilder().setColor(Colors.red_500.asColor()).setTitle("\u2757 Missing Permissions").setDescription("You do not have permission to execute the command `" + msg + "`.").build()).queue();
                    log.error("PERMS \t{}#{} <{}> tried to issued command but was missing permission \"{}\": {}", message.getAuthor().getName(), message.getAuthor().getDiscriminator(), message.getAuthor().getId(), cmd.getPermission().getName(), msg);
                    return;
                }
                log.info("{}#{} <{}> issued command: {}", message.getAuthor().getName(), message.getAuthor().getDiscriminator(), message.getAuthor().getId(), msg);
                cmd.executeCommand(message, msg);
                commands.forEach(m -> m.onCommandExecute(message, msg));
                return;
            }
        }
        // Unknown command message.
        if(getStorageManager().getStorage("configuration").getList("command-list", String.class).contains("Help") && getStorageManager().getStorage("configuration").getBoolean("unknown-command")) {
            message.getChannel().sendMessage(new EmbedBuilder().setColor(Colors.red_500.asColor()).setTitle("\u2753 Unknown command").setDescription("There is no command with the name `" + name + "`. Please do `" + prefix + "help` for more information.").build()).queue();
        }
        log.warn("Unknown command '" + name + "'.");

    }

}
