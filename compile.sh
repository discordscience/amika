#/bin/bash

# Compile in insolated conainter for replicability
docker run -it --rm -v "$PWD":/root/amika/ -v "$HOME"/.m2/:/root/.m2/ -w /root/amika/ maven:3-jdk-11-slim mvn -T10 package
